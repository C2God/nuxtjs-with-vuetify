
export const state = () =>({
    breakingBadData:[]
})

export const getters = {
    breakingBadData : state => state.breakingBadData
}

export const actions = {
    getBreakingBadData( {dispatch, commit }, params){
        return dispatch('performRequest', { path: `characters`, method: 'get', params }, { root: true }).then(response => {
            commit('setBreakingBadData', response)
            return response
          })
    }
}

export const mutations = {
    setBreakingBadData: (state, data) => (state.breakingBadData = data),
}