
export const state = () =>({
    customerData:[{
        name: "Test",
        phone: "9087654321",
        email: "test@test.com",
        address: "Street 14"
      }]
})

export const getters = {
    customerData : state => state.customerData
}

export const actions = {
    
}

export const mutations = {
    setCustomerData: (state, data) => (state.customerData = data),
}