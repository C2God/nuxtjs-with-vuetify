import axios from 'axios'

export const state = () => ({
    breakingBadApiUrl: 'https://www.breakingbadapi.com/api/'
});

export const getters = {
};

export const actions = { 
    performRequest ({ state, getters, dispatch }, data) {
        const headers = data.headers || { 'Content-Type': 'application/json' }
        let url = `${state.breakingBadApiUrl}${data.path}`
        let withCredentials = false

        return new Promise((resolve, reject) => {
            axios({
                withCredentials,
                method: data.method,
                url,
                data: data.data,
                params: data.params,
                headers
            })
                .then(resp => resolve(resp.data))
                .catch((error) => {
                    reject(error)
                })
        })
    }
};

export const mutations = {};